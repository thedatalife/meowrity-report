$(document).ready(function()
{
	// google images

	// test stuff
	var left, top;
	var column = 0;
	var row = 0;
	for(var i = 0; i < 33; i++)
	{
		left = column * 100;
		top = row * 100;
		
		column ++;
		if(column >= 11)
		{
			column = 0;
			row ++;
		}
		$("#dragtargets").append("<div style='left:"+(300 + left)+"px; top:"+(0 + top)+"px;' id='drag"+i+"' class='debugdrag leap-dragtarget'><h1>Drag</h1></div>");
	}

	// Leap init stuff
	$("#viewport").attr("width", $(window).width()).attr("height", $(window).height());
	$("#ui").width($(window).width()).height($(window).height());
	
	LeapDisplay.canvasW = $(window).width();
	LeapDisplay.canvasH = $(window).height();
	
	$(".leap-droptarget").each(function(){ LeapTarget.newDropTarget($(this)); });
	$(".leap-poketarget").each(function(){ LeapTarget.newPokeTarget($(this)); });
	$(".leap-catchtarget").each(function(){	LeapTarget.newCatchTarget($(this)); });
	$(".leap-dragtarget").each(function(){ LeapTarget.newDragTarget($(this)); });
	
	// Subscribe to custom leap events
	$(".leap-dragtarget").on('leapdrag', function(e)
	{
		console.log('leapdrag', e.draggable);
	});
	$("#upvotetarget").on('leapcatch', function(e)
	{
		e.draggable.addClass('upvote');
	});
	
	$("#downvotetarget").on('leapcatch', function(e)
	{
		e.draggable.addClass('downvote');
	});
	
	$("#saveandupvotetarget").on('leapdrop', function(e)
	{
		e.draggable.addClass('upvote');
	});
	
	$("#savetarget").on('leapdrop', function(e)
	{
		console.log("SAVE DROPPED!", e.draggable);
	});
	
	$("#poke1").on('leappoke', function(e)
	{
		console.log('leappoke');
	});
	
	LeapBuffer.initLeap();
	LeapDisplay.init(LeapBuffer, LeapTarget);
	
	var looptimer = setInterval("LeapDisplay.loop()", 20);
});