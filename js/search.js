var imageSearch;

var search = {
	
	imageindex: 0,
	imagestring: "cat meme"
	
}

function loadGSAPI() 
{
        // Create an Image Search instance.
        imageSearch = new google.search.ImageSearch();
        
        searchImages();
}
        
function searchImages()
{
        // Set searchComplete as the callback function when a search is 
        // complete.  The imageSearch object will have results in it.
        imageSearch.setSearchCompleteCallback(this, searchComplete, null);
        imageSearch.setResultSetSize(8);
        // Find me a beautiful car.
        imageSearch.execute(search.imagestring);
        
        // Include the required Google branding
        google.search.Search.getBranding('branding');
        
        search.imageindex ++;
      }
      
function searchComplete() {
	// Check that we got results
	if (imageSearch.results && imageSearch.results.length > 0) 
	{
		imageSearch.gotoPage(search.imageindex);
		search.imageindex ++;
		  // Loop through our results, printing them to the page.
		  var results = imageSearch.results;
		  for (var i = 0; i < results.length; i++) {
		    var img = document.createElement('img');
		    // There is also a result.url property which has the escaped version
		    img.src=results[i].tbUrl;
		    $("#cats").append("<div class='leap-dragtarget'>"+img+"</div>");
		  }
	}
}

google.load('search', '1');
google.setOnLoadCallback(loadGSAPI);