var LeapDisplay =
{
	falsepositivethreshold: 8, // there must be 8 samples in the buffer for a pointable id before it will display, helps reduce random flashes
	warptolerancethreshold: 200, // if the finger position jumps more than 100 units we can assume it's a glitch, helps prevent dragable objects from warping offscreen
	flickvelocitythreshold: 500,
	coastingscale: .1,
	velocityfriction: .97,
	buffer: null,
	canvasW: 800,
	canvasH: 600,
	screenscaleX: 250,
	screenscaleY: 150,
	screenscaleZ: 250,
	centerX: null,
	centerY: null,
	centerZ: null,
	canvas: null,
	ctx: null,
	
	targets: null,
	
	init: function(leapbuffer, leaptarget)
	{
		LeapDisplay.targets = leaptarget;
		LeapDisplay.buffer = leapbuffer;
		LeapDisplay.canvas = document.getElementById("viewport");
		LeapDisplay.ctx = LeapDisplay.canvas.getContext("2d");
		
		LeapDisplay.screenscaleX = $(window).width() / LeapDisplay.screenscaleX;
		LeapDisplay.screenscaleY = $(window).height() / LeapDisplay.screenscaleY;
		LeapDisplay.screenscaleZ = $(window).height() / LeapDisplay.screenscaleZ;
		
		LeapDisplay.centerX = $(window).width() * .5;
		LeapDisplay.centerY = $(window).height() * (1 + (LeapDisplay.screenscaleY / 100)) + 250;
		console.log("LeapDisplay.centerY",LeapDisplay.centerY);
		LeapDisplay.centerZ = $(window).height();
		
		console.log("screenscale = ", LeapDisplay.screenscaleX, LeapDisplay.screenscaleY, LeapDisplay.screenscaleZ);
		console.log("center = ", LeapDisplay.centerX, LeapDisplay.centerY, LeapDisplay.centerZ);

	},
	loop: function()
	{
		var data = LeapDisplay.buffer.leapdata;
		
	    LeapDisplay.canvas.width = LeapDisplay.canvas.width;
	    
	   // console.log(data);
		if(data.pointables.length > 0)
		{
			// loop through a few methods to calculate new position and set the state of the pointables and targets based on interaction
			
			LeapDisplay.pointablesScreenPositionCalculation(data.pointables);
			LeapDisplay.pointablesHitTest(data.pointables);
			LeapDisplay.drawPointables(data.pointables);
		}
		LeapDisplay.updateTargets(data.pointables);
		
	},
	
	updateTargets: function(pointables)
	{
		var pid, pindex, pointable, left, top, z, ax, ay, cx, cy;
		$(".dragging").each(function()
		{
			pid = $(this).attr('pid');
			pindex = LeapUtility.getPointableById(pointables, pid);
			pointable = pointables[pindex];
			if(typeof pointable == "undefined" || pointable.ghost)
			{
				// this means we lost the finger, remove the draggable class and connection to the pointable and exit the loop
				if($(this).hasClass('dragging')) $(this).removeClass('dragging').attr('pid', '').addClass('coasting');
			}
			else
			{
				left = pointable.screenX - $(this).attr("hitx");
				top = pointable.screenY - $(this).attr("hity");
				z = 100 + Math.abs(pointable.tipPosition[0][2]);
				
				// if the finger velocity surpasses the flick velocity threshold, change to flick
				if(Math.abs(pointable.tipVelocity[0][0]) > LeapDisplay.flickvelocitythreshold || Math.abs(pointable.tipVelocity[0][1]) > LeapDisplay.flickvelocitythreshold)
				{
					$(this).removeClass('dragging').removeClass('coasting').attr('pid', '').addClass('flicking');
					$(this).attr('vx', Number($(this).attr('vx')) * 8).attr('vy', Number($(this).attr('vy')) * 8);
				}
				// sometimes the pointable will snap somewhere offscreen, we want to prevent draggable objects from snapping with the pointable so we set max distance that the draggable can jump
				else if(Math.abs(left - $(this).position().left) < LeapDisplay.warptolerancethreshold && Math.abs(top - $(this).position().top) < LeapDisplay.warptolerancethreshold)
				{
					$(this).css("left", left + "px").css("top", top + "px");
					
					if(LeapTarget.checkForDropCollision($(this)) || LeapTarget.checkForCatchCollision($(this)))
					{
						$(this).attr('pid', -1).removeClass('dragging').addClass('dropped');
						
					}
					
					// move the anchor point until it's in the center of the object
					ax = $(this).attr("hitx");
					ay = $(this).attr("hity");
					cx = $(this).width() * .5;
					cy = $(this).height() * .5;
					
					if(ax < cx) ax ++;
					if(ax > cx) ax --;
					if(ay < cy) ay ++;
					if(ay > cy) ay --;
					
					$(this).attr("hitx", ax);
					$(this).attr("hity", ay);
					
				}
				else
				{
					$(this).removeClass('dragging').attr('pid', '');
				}
			}
		});	
		
		$(".coasting").each(function()
		{
			LeapDisplay.draggableCoasting($(this), true);
			
		});
		$(".flicking").each(function()
		{
			LeapDisplay.draggableCoasting($(this), false);
		});
	},
	
	draggableCoasting: function($this, useLimits)
	{
		
		//$this.find('h1').html($this.attr('vx') + "<br />" + $this.attr('vy'));
		var left, top;
	
		// when the velocity decreases below 1 on both axis, end the coast
		if(Math.abs(Number($this.attr('vx'))) < 1 && Math.abs(Number($this.attr('vy'))) < 1)
		{
			$this.removeClass('coasting').removeClass('flicking');
		}
		// if the velocity is too high for a limited coast, assume the finger quick shifted off the draggable
		else if(Math.abs(Number($this.attr('vx'))) > 100 || Math.abs(Number($this.attr('vy'))) > 100)
		{
			if(useLimits)
			{
				$this.removeClass('coasting').removeClass('flicking');
			}
		}
		
		// if the draggable should still be coasting, run the position calculation
		if($this.hasClass('coasting') || $this.hasClass('flicking'))
		{
			if(useLimits)
			{
				left = $this.position().left + (Number($this.attr('vx')) * LeapDisplay.coastingscale);
				top = $this.position().top - (Number($this.attr('vy')) * LeapDisplay.coastingscale);
			}
			else
			{
				left = $this.position().left + (Number($this.attr('vx')) * (LeapDisplay.coastingscale * .5));
				top = $this.position().top - (Number($this.attr('vy')) * (LeapDisplay.coastingscale * .5));
			}
			$this.css("left", left + "px").css("top", top + "px");
		}	
		
		// check if catch zone is hit
		if(LeapTarget.checkForCatchCollision($this))
		{
			$this.removeClass('coasting').removeClass('flicking').addClass('caught');
			$this.attr('vx', 0);
			$this.attr('vy', 0);
			return;
		}
		
		// update the velocity by multiplying it with the friction variable
		if(useLimits)
		{
			$this.attr('vx', Number($this.attr('vx') * LeapDisplay.velocityfriction));
			$this.attr('vy', Number($this.attr('vy') * LeapDisplay.velocityfriction));
		}
		else
		{
			$this.attr('vx', Number($this.attr('vx') * (LeapDisplay.velocityfriction * .9)));
			$this.attr('vy', Number($this.attr('vy') * (LeapDisplay.velocityfriction * .9)));
		}
	},
	
	pointablesScreenPositionCalculation: function(pointables)
	{
		for(var i = 0; i < pointables.length; i ++)
		{
			var x = Math.floor(LeapDisplay.centerX + (pointables[i].tipPosition[0][0] * LeapDisplay.screenscaleX));			
			var y = Math.floor(LeapDisplay.centerY - (pointables[i].tipPosition[0][1] * LeapDisplay.screenscaleY));
			var z = Math.floor(LeapDisplay.centerZ + (pointables[i].tipPosition[0][2] * LeapDisplay.screenscaleZ));	
			
			pointables[i].screenX = x;
			pointables[i].screenY = y;
			pointables[i].screenZ = z;
			
			if(pointables[i].screenZ > LeapDisplay.centerZ) pointables[i].ghost = true;
			else pointables[i].ghost = false;
		}
	},
	
	pointablesHitTest: function(pointables)
	{
		var hit = false;
		for(var i = 0; i < pointables.length; i ++)
		{
			hit = LeapDisplay.targets.checkForHitAtPosition(pointables[i]);
			if(hit) pointables[i].hit = true;
			else pointables[i].hit = false;
		}
	},
	
	drawPointables: function(pointables)
	{
		var color, alpha;
		for(var i = 0; i < pointables.length; i++)
		{
			if(pointables[i].bufferSize > LeapDisplay.falsepositivethreshold)
			{								
				if(pointables[i].ghost) alpha = .2;
				else alpha = 1;
				
				if(!pointables[i].ghost && pointables[i].hit) color = "200, 50, 50";
				else color = "50, 50, 200";
				
				LeapDisplay.ctx.strokeStyle = "rgba("+color+", "+alpha+")";
				
				LeapDisplay.ctx.lineWidth = 15;
				LeapDisplay.ctx.beginPath();
				LeapDisplay.ctx.arc(pointables[i].screenX, pointables[i].screenY, pointables[i].screenZ / 50, 0, 2*Math.PI);
				LeapDisplay.ctx.stroke();
			}
		}
	}
}