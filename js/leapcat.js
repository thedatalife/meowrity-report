$(document).ready(initLeapCat);

var cat = {
	
	speed: 6,
	synctime: 1000, // check for new cats every second
	flow: 10, // flow is the number of cats added to the queue every sync and the number released into the stream over the length of one sync cycle
	loop: null,
	pushcounter: 0,
	pushcounterlimit: 40,
		
	queue: [], // cat media is loaded into the queue
	stream: [],	// as needed, cat media is pushed into the stream and floats across the screen
	trash: [], // when offscreen, cat is pushed into the trash and awaits disposal
	house: [], // cats that are pulled out of the stream stay in the house until they wander to the trash
	
	emptyHouse: function()
	{
		$(".house").each(function()
		{
			console.log("empty house");
			$(this).animate({"left": $(window).width() * 2}, 600);
		});
	}
}

function initLeapCat()
{
	//cat.loop = setInterval("loop()", 42);
	$(window).on("click", toggleLoop);
}

function toggleLoop()
{
	if(cat.loop == null) cat.loop = setInterval("loop()", 42);
	else
	{
		clearTimeout(cat.loop);
		cat.loop = null;
	}
}

function loop()
{
	if(cat.queue.length > 0)
	{
		cat.pushcounter ++;
		if(cat.pushcounter >= cat.pushcounterlimit)
		{
			cat.pushcounter = 0;
			cat.pushcounterlimit += ((Math.random() * 10) - 5);
			var c = cat.queue.shift();
			
			image = jQuery("<div class='cat'></div>");
			image.append(c);
			$("#catstream").append(image);
			cat.stream.push(image);
			image.attr('w', image.width()).attr('h', image.height());
			image.css("top", Math.floor(Math.random() * 100) + "px").css("left", "-200px");
			image.animate({left: $("#catstream").width()}, (Math.random() * 10000) + 2000, trashCat);
		}
	}
	
	$(".paw").each(function()
	{
		if($(this).is(":visible") && $(this).hasClass('active'))
		{
			var position = $(this).position();
			$(this).hide(); // hide so it will not be selected with elementFromPoint
			var target = $(document.elementFromPoint(position.left, position.top));
			$(this).show(); // show right after, user doesn't see a thing... logic + magic = win
			
			if(typeof target !== 'undefined')
			{
				var whileexit = 0;
				var whileexitlimit = 5;
				var validtarget = false;
				while(!target.hasClass('cat'))
				{
					whileexit ++;
					if(whileexit >= whileexitlimit) return;
					else
					{
						target = target.parent();
						if(target.hasClass('cat')) validtarget = true;
					}
				}
				
				if(validtarget)
				{
					target.stop();
					// add the cat to the house array under the id of the paw interacting with it
					cat.house[$(this).attr('rel')] = target;
					target.addClass("house");
					target.bringToTop();
				}
			}
			
		}
	});
	
	if(cat.house.length > 0)
	{	
	console.log(cat.house);
		for(key in cat.house)
		{
			var c = cat.house[key];
			var p = $(".paw[rel='"+key+"']");
			
			if(p.is(":visible") && p.hasClass('active'))
			{
				var nx = p.position().left - (p.width() * .5);
				var ny = p.position().top - (p.height() * .5);
			
				c.css("left", nx + "px");
				c.css("top", ny + "px");
				
				var nw = (1 + (parseInt(p.attr('z')) / -32)) * (parseInt(c.attr('w')) * .75);
				//var nh = (1 + (parseInt(p.attr('z')) / -255)) * parseInt(c.attr('h')));
								
				c.find('img').css("width", nw + "px");
				c.find('img').css("height", "auto");
			}
			else
			{
				delete cat.house[key];
			}
		}
	}
}

function trashCat(e)
{
	
}

(function() {
    var highest = 1;

    $.fn.bringToTop = function() {
        this.css('z-index', ++highest); // increase highest by 1 and set the style
    };
})();
