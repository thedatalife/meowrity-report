(function(window){
	
	var ws;

	// Support both the WebSocket and MozWebSocket objects
	if ((typeof(WebSocket) == 'undefined') &&
	    (typeof(MozWebSocket) != 'undefined')) {
	  WebSocket = MozWebSocket;
	}
	
	window.onload = function()
	{
		initLeap();
	}
	
	// Create the socket with event handlers
	function initLeap() {
	  //Create and open the socket
	  ws = new WebSocket("ws://localhost:6437/");
	  
	  // On successful connection
	  ws.onopen = function(event) {

	  };
	  
	  // On message received
	  ws.onmessage = function(event) {
	    var obj = JSON.parse(event.data);
	    var str = JSON.stringify(obj, undefined, 2);
	    parseLeapEvent(obj);
	  };
	  
	  // On socket close
	  ws.onclose = function(event) {
	    ws = null;

	  }
	  
	  //On socket error
	  ws.onerror = function(event) {
	    alert("Received error");
	  };
	  	}
	
})(window);


var pointers = [];

function pointer(id)
{
	this.id = id;
	this.visible = false;
	this.paw = jQuery("<div rel='"+this.id+"' class='paw'></div>");
} 

function parseLeapEvent(obj)
{
	if(typeof obj['pointables'] !== 'undefined' && obj['pointables'].length > 0)
	{
		if(obj['pointables'].length > 7)
		{
			cat.emptyHouse();
			return;
		}
			
		var screenscalex = $(window).width() / 350;
		var screenscaley = $(window).height() / 250;
		
		var cx = $(window).width() * .5;
		var cy = $(window).height();
		
		var nx, ny, target, paw, p;
	
		$(".paw").hide();
	
		for(var i = 0; i < obj['pointables'].length; i++)
		{
			if(typeof pointers[obj['pointables'][i].id] === 'undefined')
			{
				p = new pointer(obj['pointables'][i].id);
				pointers[obj['pointables'][i].id] = p;
				$("#paws").append(p.paw);
			}
			target = obj['pointables'][i];
			nx = cx + (target.tipPosition[0] * screenscalex);
			ny = cy - (target.tipPosition[1] * screenscaley);
			
			paw = $(".paw[rel='"+obj['pointables'][i].id+"']");
			paw.empty().append("<div class='debug'><p>pointerid : "+target.id+"</p><p>handid : "+target.handId+"</p><p>length : "+target.length+"</p><p>x : "+target.tipPosition[0]+"</p><p>y : "+target.tipPosition[1]+"</p><p>z : "+target.tipPosition[2]+"</p><p>vx : "+target.tipVelocity[0]+"</p><p>vy : "+target.tipVelocity[1]+"</p><p>vz : "+target.tipVelocity[2]+"</p><p>dx : "+target.direction[0]+"</p><p>dy : "+target.direction[1]+"</p><p>dz : "+target.direction[2]+"</p><p>tool : "+target.tool+"</div>");
			
			if(target.tipPosition[2] < -20) paw.addClass('active');
			else paw.removeClass('active');
			
			paw.css("left", nx + "px");
			paw.css("top", ny + "px");
			paw.attr("z", Math.floor(target.tipPosition[2]));
			paw.show();
		}
	}
}