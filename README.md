Meowrity Report - Leap Motion Media Interface
By Miles Ryan
Requires jQuery 1.9 right now...

Consists of a few JavaScript objects to help manage the leap data, display and interaction.


js/LeapBuffer.js – 

Takes in data and buffers it based on the unique id of each hand and pointable. Parses incoming data and adds it to a buffer object that can be accessed by the display and logic layers. Because the buffer object is never fully overwritten, it can be used by each layer to store changes in state and other pieces of metadata for a visible pointable.


js/LeapTarget.js – 

Provides a way to manage interactive objects in the ui or viewport. Also provides hit testing for visible pointables.


js/LeapDisplay.js –

Reads from the leap buffer and draws the pointables. This object is responsible for driving the program loop.







--- SIMPLE EXAMPLE ---

Set up a canvas with id of viewport
Set up div with id of ui and populate with ui elements


--- HTML ---

<canvas id="viewport" width="800" height="600" style="border:1px solid #000000;"></canvas>

<div id="ui">

	<div id="upvotetarget" class="leap-droptarget">

    	<h1>upvote</h1>

	</div>

	<div id="downvotetarget" class="leap-droptarget">

    	<h1>downvote</h1>

	</div>

	<div id="saveandupvotetarget" class="leap-droptarget">

    	<h1>save and upvote</h1>

	</div>

	<div id="savetarget" class="leap-droptarget">

    	<h1>save</h1>

	</div>

</div>


---- JS ----

/*
$(document).ready(function()
{
	$("#viewport").attr("width", $(window).width()).attr("height", $(window).height());
	$("#ui").width($(window).width()).height($(window).height());
	
	LeapDisplay.canvasW = $(window).width();
	LeapDisplay.canvasH = $(window).height();
	
	LeapTarget.newDropTarget($("#upvotetarget"));
	LeapTarget.newDropTarget($("#downvotetarget"));
	LeapTarget.newDropTarget($("#saveandupvotetarget"));
	LeapTarget.newDropTarget($("#savetarget"));
	
	LeapBuffer.initLeap();
	LeapDisplay.init(LeapBuffer, LeapTarget);
	
	var looptimer = setInterval("LeapDisplay.loop()", 20);
});
*/

--- proper citation ---

Using the wonderful HTML5 Boilerplate to help power this magical experience

# [HTML5 Boilerplate](http://html5boilerplate.com)

HTML5 Boilerplate is a professional front-end template for building fast,
robust, and adaptable web apps or sites.

This project is the product of many years of iterative development and combined
community knowledge. It does not impose a specific development philosophy or
framework, so you're free to architect your code in the way that you want.

* Source: [https://github.com/h5bp/html5-boilerplate](https://github.com/h5bp/html5-boilerplate)
* Homepage: [http://html5boilerplate.com](http://html5boilerplate.com)
* Twitter: [@h5bp](http://twitter.com/h5bp)

